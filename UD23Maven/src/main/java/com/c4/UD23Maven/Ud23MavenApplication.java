package com.c4.UD23Maven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud23MavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ud23MavenApplication.class, args);
	}

}
